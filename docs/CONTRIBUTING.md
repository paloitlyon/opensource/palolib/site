# CONTRIBUTING

## Introduction

First off, thank you for considering contributing to our PALO Library. We are really excited about sharing what we have learned through our journey, but also to read and learn from others. It's people like you that make PaloLib such a great ressource.

Before submitting your contribution, please make sure to take a moment and read through the following guidelines. It contains all what you need to know, to receive a quick feedback on your issue from the community, or to participate the right way.

There are many ways to contribute, from writing new documentation, or article on a particular technology / tool that you used, improving the existing by proofreading, translating, or updating contents. 

## Ground Rules

Ensure that the following points are respected :

* **MOST IMPORTANT :** you have to be the *original author of* / have *the necessary rights on* the work that you want to share on this repo. If there is **any doubt**, the files **will be erase** without further notice.

* All commited files in this project must be written in a pure markdown syntax. You can read more about that on the [VuePress website](https://v2.vuepress.vuejs.org/guide/markdown.html#syntax-extensions). If you need any extension, please, fill an issue about it.
  
  Those MDs files can be edited directly inside your favorite IDE, or with a dedicated app such as [Mark Text](https://marktext.app/) (open source & cross-platform).

* English should be used as the primary language, so your contribution benefits to all Palowans, and readers around the world.

* All content on this repository is published under the license [Creative Commons Attribution - Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/), so please, be sure that your commited work is compatible with it.

* Be welcoming to newcomers and encourage diverse new contributors from all backgrounds. See our [Code of Conduct](CODE_OF_CONDUCT.md)

## Getting started

This library is structured with shelves, books and pages. For example :

> The shelf `Programming Language` contains a `Javascript` book, inside of it you can read a page dedicated to `VueJS`

### Issue reporting guidelines

#### 1.Create or update content

Before creating an issue, please do the following :

- Check the [existing issues](https://gitlab.com/paloitlyon/opensource/palolib/library/-/issues) to make sure you are not duplicating one
- Verify, that the issue you are about to report does not relate to any protected work

##### Content does not exist :

You can file a new issue in GitLab, describing what you will write (so others know what you are working on), and mark it with a `library: writing` label.

Then you have to create the corresponding merge request and branch, so you can work locally on your new page.

**Where applicable, please give credits and source every ressource you used !**

##### Content exists :

You can contact the author to co-edit the page with him.

Please remember to edit the corresponding book and shelf markdown files, to create the link to your page.

When the page is ready, you can send your merge request and change the label of your issue to `library: ready`

#### 2.Ask for help

Use label `library: proofreading` or `library: help` if you need help on your page.

#### 3.Request new content

Create an issue and use label `library: request` in order to request a page on a specific matter. Describe what your needs are, it may be in the scope of another palowan.

### Commit message guidelines

#### 1.Commit message format

Each commit message consists of a **header**, a **body** and a **footer**. The header has a special format that includes a **type**, a **scope** and a **subject**:

> -type-(-scope-): -subject-
> -BLANK LINE-
> -body-
> -BLANK LINE-
> -footer-

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer than 100 characters! This allows the message to be easier to read on GitLab as well as in various git tools.

Example : 

> new(page): Add Contributing guidelines
> 
> Add guidelines to help contributors to find their ways on how to
> 
> participate.
> 
> Add CC-By-SA 4.0 license for the whole content on the PALO Library.

#### 2.Revert

If the commit reverts a previous commit, it should begin with `revert:`, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

#### 3.Type

Must be one of the following:

- **new**: A new page
- **fix**: A typo fix
- **update**: An update on an existing page

#### 4.Scope

The scope could be anything specifying place of the commit change.

The valid scopes are:

- `shelf` for any changes concerning the shelves
- `book` for any changes concerning the books
- `page` for any changes concerning a page

#### 5.Subject

The subject contains succinct description of the change:

- use the imperative, present tense: "change" not "changed" nor "changes"
- capitalize first letter
- no dot (.) at the end
- should not be more than 50 characters

#### 6.Body

Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes". The body should include the motivation for the change and contrast this with previous behavior.
Wrap it at 72 characters max.

#### 7.Footer

The footer should contain any information about **Breaking Changes** and is also the place to reference GitLab issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.

### Page Model

Please follow this kind of pattern when creating content : 

> # TITLE h1
> 
> ## Main Subtitle 1 h2
> 
> Paragraph Lorem ipsum
> 
> ### Section Subtitle 1 h3
> 
> ### Section Subtitle 2 h3
> 
> Paragraph Lorem ipsum
> 
> ## Main Subtitle 2 h2
> 
> ### Section Subtitle 1 h3
> 
> Paragraph Lorem ipsum