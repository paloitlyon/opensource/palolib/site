---

home: true
heroImage: '/assets/hero.png'
heroText: 'Welcome on PaloLib'
actions:

- text: Library →
  link: /library/
  type: primary
- text: How to contribute
  link: /CONTRIBUTING
  type: secondary
  features:
  - title: Get a book
    details: Explore our library to find ressources on a specific topic.
  - title: Give your input
    details: Contribute to our library, by editing or giving your opinion on a subject. 
  - title: Write a book
    details: Join the journey, and write some awesome articles.
    footer: Choose a license | Copyright © 2021-present PALO IT

footer: '<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a> License on all articles | A <a href="https://www.palo-it.com">PALO IT</a> initiative'
footerHtml: true

---