const { resolve } = require('path')

module.exports = {
  lang: 'en-US',
  title: 'PaloLib',
  description: 'PALO IT collaborative library & curated documentation',
  base: '/opensource/palolib/site/',
  dest: 'public',
  themeConfig: {
    logo: '/assets/colour_logo.svg',
    navbar: [
      { text: 'Home', link: '/' },
      { text: 'Library', link: '/library/' },
      { text: 'Contributing', link: '/contributing' }
    ],
    repo: 'https://gitlab.com/paloitlyon/opensource/palolib',
    sidebar: 'auto'
  },

  bundlerConfig: {
    viteOptions: {
      resolve: {
        alias: {
          '@img': resolve(__dirname, './public/img')
        }
      }
    },
  },

  plugins: [
    [
      '@vuepress/plugin-search',
      {
        locales: {
          '/': {
            placeholder: 'Search',
          },
          '/zh/': {
            placeholder: '搜索',
          },
        },
      },
    ],
    [
      '@vuepress/plugin-shiki',
      {

        theme: 'one-dark-pro'
      }
    ]
  ]
}